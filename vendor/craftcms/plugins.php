<?php

$vendorDir = dirname(__DIR__);

return array (
  'craftcms/query' => 
  array (
    'class' => 'craft\\query\\Plugin',
    'basePath' => $vendorDir . '/craftcms/query/src',
    'handle' => 'query',
    'aliases' => 
    array (
      '@craft/query' => $vendorDir . '/craftcms/query/src',
    ),
    'name' => 'Query',
    'version' => '2.0.2',
    'description' => 'Execute database queries from the Control Panel',
    'developer' => 'Pixel & Tonic',
    'developerUrl' => 'https://pixelandtonic.com/',
    'changelogUrl' => 'https://raw.githubusercontent.com/craftcms/query/v2/CHANGELOG.md',
    'downloadUrl' => 'https://github.com/craftcms/query/archive/v2.zip',
  ),
  'nystudio107/craft3-minify' => 
  array (
    'class' => 'nystudio107\\minify\\Minify',
    'basePath' => $vendorDir . '/nystudio107/craft3-minify/src',
    'handle' => 'minify',
    'aliases' => 
    array (
      '@nystudio107/minify' => $vendorDir . '/nystudio107/craft3-minify/src',
    ),
    'name' => 'Minify',
    'version' => '1.2.6',
    'schemaVersion' => '1.0.0',
    'description' => 'A simple plugin that allows you to minify blocks of HTML, CSS, and JS inline in Craft CMS templates.',
    'developer' => 'nystudio107',
    'developerUrl' => 'https://nystudio107.com/',
    'changelogUrl' => 'https://raw.githubusercontent.com/nystudio107/craft3-minify/CHANGELOG.md',
    'hasCpSettings' => false,
    'hasCpSection' => false,
    'components' => 
    array (
      'minify' => 'nystudio107\\minify\\services\\MinifyService',
    ),
  ),
  'craftcms/aws-s3' => 
  array (
    'class' => 'craft\\awss3\\Plugin',
    'basePath' => $vendorDir . '/craftcms/aws-s3/src',
    'handle' => 'aws-s3',
    'aliases' => 
    array (
      '@craft/awss3' => $vendorDir . '/craftcms/aws-s3/src',
    ),
    'name' => 'Amazon S3',
    'version' => '1.0.8',
    'description' => 'Amazon S3 integration for Craft CMS',
    'developer' => 'Pixel & Tonic',
    'developerUrl' => 'https://pixelandtonic.com/',
    'changelogUrl' => 'https://raw.githubusercontent.com/craftcms/aws-s3/master/CHANGELOG.md',
    'downloadUrl' => 'https://github.com/craftcms/aws-s3/archive/master.zip',
  ),
  'solspace/craft3-freeform' => 
  array (
    'class' => 'Solspace\\Freeform\\Freeform',
    'basePath' => $vendorDir . '/solspace/craft3-freeform/src',
    'handle' => 'freeform',
    'aliases' => 
    array (
      '@Solspace/Freeform' => $vendorDir . '/solspace/craft3-freeform/src',
    ),
    'name' => 'Freeform Lite',
    'version' => '2.0.0-beta.5',
    'schemaVersion' => '2.0.1',
    'description' => 'The most intuitive and powerful form builder for Craft.',
    'developer' => 'Solspace',
    'developerUrl' => 'https://solspace.com/craft/freeform',
    'documentationUrl' => 'https://solspace.com/craft/freeform/docs',
    'changelogUrl' => 'https://raw.githubusercontent.com/solspace/craft3-freeform/master/CHANGELOG.md',
    'hasCpSection' => true,
  ),
  'craftcms/redactor' => 
  array (
    'class' => 'craft\\redactor\\Plugin',
    'basePath' => $vendorDir . '/craftcms/redactor/src',
    'handle' => 'redactor',
    'aliases' => 
    array (
      '@craft/redactor' => $vendorDir . '/craftcms/redactor/src',
    ),
    'name' => 'Redactor',
    'version' => '1.0.1',
    'description' => 'Edit rich text content in Craft CMS using Redactor by Imperavi.',
    'developer' => 'Pixel & Tonic',
    'developerUrl' => 'https://pixelandtonic.com/',
    'changelogUrl' => 'https://raw.githubusercontent.com/craftcms/redactor/master/CHANGELOG.md',
    'downloadUrl' => 'https://github.com/craftcms/redactor/archive/master.zip',
  ),
  'acultimates/actemplate-cache' => 
  array (
    'class' => 'acultimates\\actemplatecache\\ACTemplateCache',
    'basePath' => $vendorDir . '/acultimates/actemplate-cache/src',
    'handle' => 'actemplate-cache',
    'aliases' => 
    array (
      '@acultimates/actemplatecache' => $vendorDir . '/acultimates/actemplate-cache/src',
    ),
    'name' => 'ACTemplateCache',
    'version' => '1.0.0',
    'schemaVersion' => '1.0.0',
    'description' => 'A simple plugin for EEWeb template caches.',
    'developer' => 'AspenCore Ultimates',
    'developerUrl' => 'https://bitbucket.org/ac-ultimates',
    'documentationUrl' => 'https://github.com/acultimates/actemplate-cache/blob/master/README.md',
    'changelogUrl' => 'https://raw.githubusercontent.com/acultimates/actemplate-cache/master/CHANGELOG.md',
    'hasCpSettings' => true,
    'hasCpSection' => true,
    'components' => 
    array (
      'aCTemplateCacheService' => 'acultimates\\actemplatecache\\services\\ACTemplateCacheService',
    ),
  ),
  'craftcms/contact-form' => 
  array (
    'class' => 'craft\\contactform\\Plugin',
    'basePath' => $vendorDir . '/craftcms/contact-form/src',
    'handle' => 'contact-form',
    'aliases' => 
    array (
      '@craft/contactform' => $vendorDir . '/craftcms/contact-form/src',
    ),
    'name' => 'Contact Form',
    'version' => '2.1.1',
    'description' => 'Add a simple contact form to your Craft CMS site',
    'developer' => 'Pixel & Tonic',
    'developerUrl' => 'https://pixelandtonic.com/',
    'developerEmail' => 'support@craftcms.com',
    'documentationUrl' => 'https://github.com/craftcms/contact-form',
    'changelogUrl' => 'https://raw.githubusercontent.com/craftcms/contact-form/v2/CHANGELOG.md',
    'downloadUrl' => 'https://github.com/craftcms/contact-form/archive/v2.zip',
    'components' => 
    array (
      'mailer' => 'craft\\contactform\\Mailer',
    ),
  ),
  'acultimates/acengineerspicks' => 
  array (
    'class' => 'acultimates\\acengineerspicks\\ACEngineersPicks',
    'basePath' => $vendorDir . '/acultimates/acengineerspicks/src',
    'handle' => 'acengineerspicks',
    'aliases' => 
    array (
      '@acultimates/acengineerspicks' => $vendorDir . '/acultimates/acengineerspicks/src',
    ),
    'name' => 'AC Engineers Picks',
    'version' => '1.0.0',
    'schemaVersion' => '1.0.0',
    'description' => 'A simple plugin for listing Engineer\'s Picks entries.',
    'developer' => 'AspenCore Ultimates',
    'developerUrl' => 'https://bitbucket.org/ac-ultimates',
    'hasCpSettings' => true,
    'hasCpSection' => true,
  ),
);

Query for Craft CMS
===================

This plugin enables users to execute database queries and view their results in [Craft CMS](https://craftcms.com).

## Requirements

This plugin requires Craft CMS 3.0.0-beta.1 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require craftcms/query

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for Query.

4. Go to “Utilities” in the Control Panel to execute queries database queries.
